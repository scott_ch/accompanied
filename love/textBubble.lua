
local class = require "class"

TextBubble = class(function(self,text,x,y,maxwidth,fontsize,orient)
  self.text = text
  self.x = x
  self.y = y
  self.typingText = 0
  self.toType = ""
  self.maxwidth = maxwidth
  self.orient = orient
  
  self.xD = 0
  self.yD = 0
  
  --figure out how to format text to fit within maxwidth.
  --do constructively:
  
  self.border = 10
  
  self.font = love.graphics.newFont( fontsize )
  love.graphics.setFont(self.font)
  self.fontsize = fontsize
  self.printText = ""
  self.curLine = ""
  self.numLines = 1
  self.maxseen = 0
  
  for word in string.gmatch(self.text, "%a+") do
	self.width = self.font:getWidth(self.curLine..word)
	if(self.width>self.maxwidth) then
		self.printText = self.printText.."\n"..word.." "
		self.curLine = word.." "
		self.maxseen = math.max(self.maxseen,self.font:getWidth(self.curLine))
		self.numLines = self.numLines + 1
	else
		self.printText = self.printText..word.." "
		self.curLine = self.curLine..word.." "
		self.maxseen = math.max(self.maxseen,self.width)
	end
 end
 
-- self.maxseen = math.max(self.maxseen,self.width)
 
 self.maxwidth = self.maxseen
  
  self.width = self.maxwidth + self.border*2
  self.height = self.numLines*self.fontsize*1.2 + self.border*2
  
  --self.printText = text
  
end)


function TextBubble:setText(inText)
self.text = inText
self.printText = ""
  self.curLine = ""
    self.numLines = 1
  self.maxseen = 0
    for word in string.gmatch(inText, "%a+") do
	self.width = self.font:getWidth(self.curLine..word)
	if(self.width>self.maxwidth) then
		self.printText = self.printText.."\n"..word.." "
		self.curLine = word.." "
		self.maxseen = math.max(self.maxseen,self.font:getWidth(self.curLine))
		self.numLines = self.numLines + 1
	else
		self.printText = self.printText..word.." "
		self.curLine = self.curLine..word.." "
		self.maxseen = math.max(self.maxseen,self.width)
	end
 end
 
-- self.maxseen = math.max(self.maxseen,self.width)
 
 self.maxwidth = self.maxseen
  
  self.width = self.maxwidth + self.border*2
  self.height = self.numLines*self.fontsize*1.2 + self.border*2
end

function TextBubble:setFontSize(newSize)

self.fontsize = newSize
self.font = love.graphics.newFont( newSize )

if(self.typingText==0) then
self:setText(self.text)
end
--  love.graphics.setFont(self.font)
  
  end

function TextBubble:typeText(toType)
	self.typingText = 1
	self.toType = toType
end


function TextBubble:setX(x)
  self.x = x
end

function TextBubble:setY(y)
  self.y = y
end

function TextBubble:tForce(oB)
	xcenDiff = (oB.x + oB.width/2) - (self.x + self.width/2)
	ycenDiff = (oB.y + oB.height/2) - (self.y + self.height/2)
	
	xwidDiff = oB.width/2 + self.width/2
	ywidDiff = oB.height/2 + self.height/2
	
	if(math.abs(xcenDiff)<xwidDiff and math.abs(ycenDiff)<ywidDiff) then
		self.xD = self.xD + math.pow(xcenDiff/xwidDiff,1)
		self.yD = self.yD + math.pow(ycenDiff/ywidDiff,1)
	end
end

function TextBubble:setPos(x,y)
--set new location of bubble stalk
	self.x = self.x - self.xD
	self.y = self.y - self.yD
	self.xD = 0
	self.yD = 0

  self.xS = x
  self.yS = y

  
  mD = -30
  
--set new location of bubble top xy
--move x and y for box to be ptLen away from xS and yS

	if self.xS - self.x < mD then
		self.x = self.xS - mD
	end
	
	if self.x + self.width - self.xS < mD then
		self.x = self.xS -self.width + mD
	end
	
	if self.yS - self.y < mD then
		self.y = self.yS - mD
	end
	
	if self.y + self.height - self.yS < mD then
		self.y = self.yS -self.height + mD
	end

--self.width

  --self.x = x
  --self.y = y
  
  --self.x + self.width/2
  
  
  self.xSC1 = self.x + self.width/3
  self.ySC1 = self.y + self.height/3
  self.xSC2 = self.x + self.width*2/3
  self.ySC2 = self.y + self.height*2/3
  
  self.xSC3 = self.x + self.width/3
  self.ySC3 = self.y + self.height*2/3
  self.xSC4 = self.x + self.width*2/3
  self.ySC4 = self.y + self.height/3
  
  
  
end

function TextBubble:getPos()
	return self.xS, self.yS
end

function TextBubble:set_card_audio(audio)
  self.audio = audio
end

function TextBubble:set_value(value)
  self.value = value
end

function TextBubble:set_position(x, y)
  self.x = x
  self.y = y
end

function TextBubble:set_bounds(width, height)
  self.width = width
  self.height = height
end

function TextBubble:flip()
  self.flipped = not self.flipped
end

function TextBubble:remove()
  self.valid = false
end

function TextBubble:draw()



if(self.typingText>0) then
self.printText = ""
  self.curLine = ""
    self.numLines = 1
  self.maxseen = 0
    for word in string.gmatch(string.sub(self.toType,1,self.typingText), "%a+") do
	self.width = self.font:getWidth(self.curLine..word)
	if(self.width>self.maxwidth) then
		self.printText = self.printText.."\n"..word.." "
		self.curLine = word.." "
		self.maxseen = math.max(self.maxseen,self.font:getWidth(self.curLine))
		self.numLines = self.numLines + 1
	else
		self.printText = self.printText..word.." "
		self.curLine = self.curLine..word.." "
		self.maxseen = math.max(self.maxseen,self.width)
	end
 end
 
-- self.maxseen = math.max(self.maxseen,self.width)
 
 self.maxwidth = self.maxseen
  
  self.width = self.maxwidth + self.border*2
  self.height = self.numLines*self.fontsize*1.2 + self.border*2
  
  self.typingText = self.typingText + 1
  
  if(self.typingText > self.toType:len()) then
  self.typingText = 0
  end
  
end



	--width = Font:getWidth(self.text)

	love.graphics.setColor(50, 50, 50,120)
	love.graphics.rectangle("fill", self.x, self.y, self.width, self.height, 10, 10, 5 )
	
	love.graphics.polygon('fill', self.xSC1, self.ySC1, self.xS, self.yS, self.xSC2, self.ySC2)
	love.graphics.polygon('fill', self.xSC3, self.ySC3, self.xS, self.yS, self.xSC4, self.ySC4)
	
	
	love.graphics.setColor(255, 255, 255)
	love.graphics.setFont(self.font)
    love.graphics.print(self.printText, self.x + self.border, self.y + self.border)
end

return TextBubble