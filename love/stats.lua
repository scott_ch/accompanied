class = require "class"

local Stats = class(function (self)
  self.prisoners_released = 0
  self.total_prisoners = 0
  self.prisoners_dead = 0
  self.keys = 0

  -- drawing
  self.font = love.graphics.newFont(30)
  self.text = love.graphics.newText(self.font, "Status: ")
end)


function Stats:draw_hud()
  love.graphics.setColor(255, 255, 255)
  --love.graphics.printf("" .. self.prisoners_released .. " / " .. self.total_prisoners,
  --  10, love.graphics.getHeight() - 20, 50, "left")
  self.text:set("Prisoners released: " .. self.prisoners_released .. " / " .. self.total_prisoners .. " | " .. "Prisoners dead: " .. self.prisoners_dead .. " / " .. self.total_prisoners .. " | " .. "Keys: " .. self.keys)
  love.graphics.draw(self.text, 10, love.graphics.getHeight() - 50)
end

return {Stats = Stats}
