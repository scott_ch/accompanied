function evalfile(filename, env)
    local f = assert(loadfile(filename))
    return f()
end

function eval(text)
    local f = assert(load(text))
    return f()
end

function errorhandler(err)
    return debug.traceback(err)
end

function include(filename)
    local success, result = xpcall(evalfile, errorhandler, filename)

    --print(string.format("success=%s filename=%s\n", success, filename))
    if not success then
        print("[ERROR]\n",result,"[/ERROR]\n")
    end
end

function include_noerror(filename)
    local success, result = xpcall(evalfile, errorhandler, filename)
    --print(string.format("success=%s filename=%s\n", success, filename))
end

return {evalfile = evalfile, eval = eval, errorhandler = errorhandler, include=include, include_noerror=include_noerror}
