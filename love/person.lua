
local class = require "class"

local Person = class(function(self, name, world)
  self.name = name
  self.radius = 20 -- 64/2
  self.dir = 0
  self.has_gun = false
  self.has_key = false
  self.imprisoned = false
  self.released = false
  self.font = love.graphics.newFont( 10 )

  love.graphics.setFont(self.font)

  -- physics
  self.body = love.physics.newBody(world, 0, 0, "dynamic")
  self.shape = love.physics.newCircleShape(self.radius) --the ball's shape has a radius of 20
  self.fixture = love.physics.newFixture(self.body, self.shape, 2) -- Attach fixture to body and give it a density of 1.
  self.body:setLinearDamping(10)
  self.body:setAngularDamping(10)

  -- drawing
  self.hair = love.graphics.newImage("sprites/person/hair/hair3.png")
  self.head = love.graphics.newImage("sprites/person/head/head1.png")
  self.shoulders = love.graphics.newImage("sprites/person/shoulders/shoulders1.png")
  self.gun = love.graphics.newImage("sprites/person/guns/guns1.png")
  self.key = love.graphics.newImage("sprites/person/key/key1.png")

  --AI
  self.follow_target = nil
end)

function Person.make_prisoner(name, word)
  local p = Person(name, world)
  p.imprisoned = true
  return p
end

function Person.make_guard(name, word)
  local p = Person(name, world)
  p.has_key = true
  return p
end

function Person:draw()

  if self.released then
    return
  end

  if self.has_gun then
    love.graphics.draw(self.gun, self.body:getX(), self.body:getY(), self.dir, 1, 1, self.gun:getWidth()/2, self.gun:getHeight()/2)
  end


  love.graphics.draw(self.shoulders, self.body:getX(), self.body:getY(), self.dir, 1, 1, self.shoulders:getWidth()/2, self.shoulders:getHeight()/2)
  love.graphics.draw(self.head, self.body:getX(), self.body:getY(), self.dir, 1, 1, self.head:getWidth()/2, self.head:getHeight()/2)


  --love.graphics.circle("fill", self.body:getX(), self.body:getY(), 10, 8)
  love.graphics.draw(self.hair, self.body:getX(), self.body:getY(), self.dir, 1, 1, self.hair:getWidth()/2, self.hair:getHeight()/2)

if self.has_key then
  love.graphics.draw(self.key, self.body:getX(), self.body:getY(), self.dir, 1, 1, self.key:getWidth()/2, self.key:getHeight()/2)
end
  --self.font = love.graphics.newFont( fontsize )
  --love.graphics.setFont(self.font)
  love.graphics.setFont(self.font)
  love.graphics.printf(self.name, self.body:getX() - 100, self.body:getY() + self.radius, 200, "center")
end

function Person:getX()
  return self.body:getX()
end

function Person:getY()
  return self.body:getY()
end

function Person:getAngle()
  return self.body:getAngle()
end

function Person:move_forward_in_direction(force)
  self.body:applyForce(force*math.cos(self.body:getAngle()), force*math.sin(self.body:getAngle()))
end

function Person:move_backward_in_direction()
  self.body:applyForce(-Person.config.force_to_apply*math.cos(self.body:getAngle()), -Person.config.force_to_apply*math.sin(self.body:getAngle()))
end

function Person:rotate_left()
  self.body:applyTorque(-100)
end

function Person:rotate_right()
  self.body:applyTorque(100)
end

function Person:move_up()
  self.body:applyForce(0, -Person.config.force_to_apply)
end

function Person:move_down()
  self.body:applyForce(0, Person.config.force_to_apply)
end

function Person:move_left()
  self.body:applyForce(-Person.config.force_to_apply, 0)
end

function Person:move_right()
  self.body:applyForce(Person.config.force_to_apply, 0)
end

function Person:face(x, y)
  self.dir = math.atan2(y - self.body:getY(), x - self.body:getX())
end

function Person:follow(person)
  self.follow_target = person
end

function sgn(x)
  if x >= 0 then
    return 1
  else
    return -1
  end
end

function shortest_angle_betw(theta1, theta2)
  if math.abs(theta1 - theta2) < 2*math.pi - math.abs(theta1 - theta2) then
    return theta1 - theta2
  else
    return (2*math.pi - math.abs(theta1 - theta2)) * sgn(theta2 - theta1)
  end
end

--function shortest_angle_betw(theta)
--  if theta > math.pi thenw
--    return 2*math.pi - theta
--  end
--  return theta
--end

function Person:update_prisoner_ai()
  if not self.imprisoned and self.follow_target ~= nil and self.follow_target.body ~= nil then

    local angle_betw = shortest_angle_betw(math.atan2(self.follow_target:getY() - self:getY(), self.follow_target:getX() - self:getX()), self.dir)
    self.dir = self.dir + (angle_betw) * 0.05

    if self.dir > math.pi then
      self.dir = self.dir - 2*math.pi
    elseif self.dir < -math.pi then
      self.dir = self.dir + 2*math.pi
    end

    if math.abs(angle_betw) < math.pi/45 then
      -- we are within ~ 4 degrees and not too close

      self.body:setAngle(self.dir)


      local dist_betw = math.sqrt(math.pow(self.follow_target:getX() - self:getX(), 2) + math.pow(self.follow_target:getY() - self:getY(), 2))
      local multiplier
      if dist_betw < 1000 then -- FIXME: generic magic number
        multiplier = (dist_betw)/1000
      else
        multiplier = 1
      end
      self:move_forward_in_direction(Person.config.force_to_apply*multiplier)

    end

  end
end

return {Person = Person}
