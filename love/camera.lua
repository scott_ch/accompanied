local class = require "class"

local Camera = class(function(self)
  self.translation = {x = 0, y = 0}
  self.screen_centre = {x = love.graphics:getWidth()/2, y = love.graphics:getHeight()/2}
end)

function Camera:update(person)
  self.translation.x = self.translation.x + (person.body:getX() - self.translation.x - self.screen_centre.x)*0.05
  self.translation.y = self.translation.y + (person.body:getY() - self.translation.y - self.screen_centre.y)*0.05
end

function Camera:use()
  love.graphics.translate(-self.translation.x, -self.translation.y)
end

function Camera:local_to_world(local_x, local_y)
  return {x = local_x + self.translation.x, y = local_y + self.translation.y}
end

function Camera:world_to_local(world_x, world_y)
  return {x = world_x - self.translation.x, y = world_y - self.translation.y}
end

return {Camera = Camera}
