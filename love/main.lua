
local textBubble = require "textBubble"
local map_module = require "map"
local person_module = require "person"
local camera_module = require "camera"
local reload = require "reload"
local stats_module = require "stats"
local text_source_module = require "text_source"
local state_machine_module = require "state_machine"
local menu_module = require "menu"

function load_config()
  -- reload.include("config.lua")
  package.loaded.config = nil
  require "config"

  talkData = love.sound.newSoundData("sounds/phoneyObama.mp3")

  talker = love.audio.newSource(talkData)

  talker:seek(5,"samples")

  talker:setVolume(0.9)
  talker:setPitch(1.5)
  talker:play()

  person_module.Person.config = config.person

  randomLines = {}
  numLines = 0
  local file = io.open("randomLines.txt")
	if file then
		for line in file:lines() do
			table.insert(randomLines, line)
			numLines = numLines+1
		end
	else
	end

  -- Person.config = {force_to_apply = 4}

end

function dist_obj(obj1, obj2)
  return math.sqrt(math.pow(obj1:getX() - obj2:getX(), 2) + math.pow(obj1:getY() - obj2:getY(), 2))
end

function dist_obj_xy(obj1, x, y)
  return math.sqrt(math.pow(obj1:getX() - x, 2) + math.pow(obj1:getY() - y, 2))
end

function love.load()

  game_state = state_machine_module.create({
  initial = 'splashscreen',
  events = {
    { name = 'next',  from = 'splashscreen', to = 'menu' },
    { name = 'next',  from = 'menu', to = 'game'},
    { name = 'pause',  from = 'game', to="menu"},
    { name = 'resume', from = "menu", to = 'game'},
  }})

  splash_screen_sprite = love.graphics.newImage("sprites/splash/splashscreen.png")

  menu = menu_module.Menu()

  math.randomseed(os.time())
	tB1 = TextBubble("oh",200,200,100,10,1)
love.mouse.setVisible( false )

  load_config()

  stats = stats_module.Stats()

  text_source = text_source_module.TextSource("texts/jersey_shore.txt")

sW = 1024
sH = 768

  --map_sprite = love.graphics.newImage("map/concept.png")

  love.physics.setMeter(64)
  world = love.physics.newWorld(0, 0, true)

  love.window.setMode(sW, sH)


  world = love.physics.newWorld(0, 0, true)
  map = map_module.Map()

    --map:load("map/default.map")

  --  map_sprite = love.graphics.newImage("map/prison.jpg")
  --  collisions = love.image.newImageData("map/collision.png")
  --map:load("map/prison.jpg", "map/collision.png", world)
  map:load2("map/prison.svg")






	local charset = {}

-- qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890
for i = 48,  57 do table.insert(charset, string.char(i)) end
for i = 65,  90 do table.insert(charset, string.char(i)) end
for i = 97, 122 do table.insert(charset, string.char(i)) end

function string.random(length)
  --math.randomseed(os.time())

  if length > 0 then
    return string.random(length - 1) .. charset[math.random(1, #charset)]
  else
    return ""
  end
end





  tB = {}
  for x = 1, 50 do
    tB[x] = TextBubble(string.random(50),200,200,100,10,1)
  end

  --x = 160
  --y = 1024

  person = person_module.Person("Fred", world,1)
  person.body:setX(100)
  person.body:setY(map.map_sprite:getHeight()/2)


  -- create all the prisoners
  prisoners = {}

  follow_target = person
  for i, pl in ipairs(map.prisoner_locations) do
    local prisoner = person_module.Person.make_prisoner("Prisoner", world)
    prisoner.body:setX(pl[1])
    prisoner.body:setY(pl[2])
     prisoner:follow(follow_target)
    --prisoner:follow(person)
    follow_target = prisoner
    table.insert(prisoners, prisoner)
  end

  stats.total_prisoners = #prisoners


  -- create the guards
  guards = {}
  for i, gl in ipairs(map.guard_locations) do
    local guard = person_module.Person.make_guard("Guard", world)
    guard.body:setX(gl[1])
    guard.body:setY(gl[2])
    table.insert(guards, guard)
  end

  camera = camera_module.Camera()
end


function shuffle_prisoners()
  local behaviour = math.random(1, 2)
  print(behaviour)
  if behaviour == 1 then
    -- conga line
    local follow_target = person
    for i, prisoner in ipairs(prisoners) do
      if not prisoner.imprisoned and not prisoner.released then
        prisoner:follow(follow_target)
        follow_target = prisoner
      end
    end
  elseif behaviour == 2 then
    for i, prisoner in ipairs(prisoners) do
      if not prisoner.imprisoned and not prisoner.released then
        prisoner:follow(person)
      end
    end
  end
end


function love.keypressed(key, scancode, isrepeat)
  if game_state:is("splashscreen") then
    if key == "return" and not(isrepeat) then
      game_state:next()
    end

  elseif game_state:is("menu") then
    if key == "up" and not(isrepeat) then
      menu:up()
    elseif key == "down" and not isrepeat then
      menu:down()
    elseif key == "return" and not isrepeat then
      print("select")
      menu:select()
    end
  end
end

function love.update(dt)

  if game_state:is("splashscreen") then
  elseif game_state:is("menu") then

  elseif game_state:is("game") then
    --x,y = tB1
    local mouse_in_world = camera:local_to_world(love.mouse:getX(), love.mouse:getY())
    tB1:setPos(mouse_in_world.x, mouse_in_world.y)



    world:update(dt) --this puts the world into motion

    --here we are going to create some keyboard events
    if love.keyboard.isDown("right") or love.keyboard.isDown("d") then --press the right arrow key to push the ball to the right
      person:move_right()
    elseif love.keyboard.isDown("left") or love.keyboard.isDown("a") then --press the left arrow key to push the ball to the left
      person:move_left()
    end

    if love.keyboard.isDown("up") or love.keyboard.isDown("w") then --press the up arrow key to set the ball in the air
      person:move_up()
    elseif love.keyboard.isDown("down") or love.keyboard.isDown("s") then
      person:move_down()
    end

    for x = 1, 50 do
  	if(math.random(1,100)==100) then
  		tB[x]:setFontSize(math.random(5,12))
  		tB[x]:typeText(text_source:get_line())
  		--tB[x]:typeText(randomLines[1])
  	end
    end



    for a = 1, 50 do
  	for b = 1, 50 do
  	if a ~= b then
  		tB[a]:tForce(tB[b])
  		end
  	end
    end


  	for i, prisoner in ipairs(prisoners) do
      if not prisoner.released then
  		    tB[i]:setPos(prisoner:getX(), prisoner:getY())
        end
    end




    person:face(mouse_in_world.x, mouse_in_world.y)
    camera:update(person)

    if love.keyboard.isDown("r") then
      load_config()
    end

    local do_shuffle_prisoners = false

    local released_prisoners = {}
    for i, prisoner in ipairs(prisoners) do
      if (prisoner.released or prisoner.body == nil) then
        print("Fuck...")
      end
      prisoner:update_prisoner_ai()

      local dist_betw = dist_obj(person, prisoner)

      if prisoner.imprisoned and dist_betw < 80 then
        prisoner.imprisoned = false
        do_shuffle_prisoners = true
      end

      if not prisoner.released and prisoner:getX() < 0 then
        prisoner.released = true
        prisoner.body:destroy()
        prisoner.body = nil
        stats.prisoners_released = stats.prisoners_released + 1
        do_shuffle_prisoners = true
        table.insert(released_prisoners, i)
      end
    end

    -- released_prisoners will be sorted
    for i, released_prisoner in ipairs(released_prisoners) do
      print(i, released_prisoner)
      print("removing ", released_prisoner - i + 1)
      print(prisoners[released_prisoner - i + 1].released)
      print(prisoners[released_prisoner - i + 1].body)
      table.remove(prisoners, released_prisoner- i + 1)
    end

    if do_shuffle_prisoners then
      shuffle_prisoners()
    end

    for i, prisoner in ipairs(prisoners) do
      if (prisoner.released) then
        print("Fuck")
      end
    end

    for i, guard in ipairs(guards) do
      local dist_betw = dist_obj(person, guard)
      if guard.has_key and dist_betw < config.guard.key_dist then
        guard.has_key = false
        stats.keys = stats.keys + 1
      end
    end


    for i, door in ipairs(map.doors) do
      local dist_betw = dist_obj_xy(person, door.x, door.y)
      if dist_betw < 60 and door.body ~= nil and stats.keys > 0 then
        door.body:destroy()
        door.body = nil
        stats.keys = stats.keys - 1
      end
    end
  end
end




function love.draw()

  if game_state:is("splashscreen") then
    love.graphics.draw(splash_screen_sprite, -300, 0)
  elseif game_state:is("game") or game_state:is("menu") then
    love.graphics.push()
    camera:use()

    map:draw()

    person:draw()
    love.graphics.setColor(255, 255, 255)
    for i, prisoner in ipairs(prisoners) do
      prisoner:draw()
    end

    for i, guard in ipairs(guards) do
      guard:draw()
    end

    for x = 1, #prisoners do
      tB[x]:draw()
    end

    love.graphics.pop()

    if game_state:is("menu") then
      menu:draw()
    else
      stats:draw_hud()
    end


  end


   --love.graphics.setColor(200, 200, 200)

  -- for x = 1, wallNum-1 do
  --love.graphics.polygon("fill", walls[x].body:getWorldPoints(walls[x].shape:getPoints()))
  --end


end
