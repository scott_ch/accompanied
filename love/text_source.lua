
local class = require "class"

local TextSource = class(function(self, filename)
  self.text = io.open(filename):read("*all")
  self.lines = {}
  for line in string.gmatch(self.text, "([^\n]+)") do
    table.insert(self.lines, line)
  end
end)

function TextSource:get_line()
  local line_index = math.random(1, #self.lines)
  return self.lines[line_index]
end

return {TextSource = TextSource}
