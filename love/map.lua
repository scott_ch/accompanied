
local class = require "class"

local Map = class(function(self)
  self.tile_array = {}
  self.guard_locations = {}
  self.prisoner_locations = {}
  self.doors = {}
end)

function Map:load(image_filename, collisions_filename, world)
  -- local mapfile = io.open(filename, "r")

  self.map_sprite = love.graphics.newImage("map/prison.jpg")
  self.collisions = love.image.newImageData("map/collision.png")

  self.walls = {}
  local wallNum = 1

	for x = 1, self.collisions:getWidth() do
		for y = 1, self.collisions:getHeight() do
			-- Pixel coordinates range from 0 to image width - 1 / height - 1.
			local r,g,b,a = self.collisions:getPixel( x - 1, y - 1 )

			if(r==255 and g==0 and b==0) then
				--start of vertical line. iterate downwards to find lower bound.
				offset = 0
				while (r==255 and g==0 and b==0) and x + offset + 1 <self.collisions:getWidth() do
					self.collisions:setPixel(x + offset,y-1,0,0,0,0)
					offset = offset+1
					r,g,b,a = self.collisions:getPixel( x + offset, y - 1)
				end
				self.collisions:setPixel(x + offset,y-1,0,0,0,0)
				self.walls[wallNum] = {}
				--self.walls[wallNum].body = love.physics.newBody(world, x+self.collisions:getWidth()/2, y)
				--self.walls[wallNum].shape = love.physics.newRectangleShape(offset, 1)
				--self.walls[wallNum].fixture = love.physics.newFixture(self.walls[wallNum].body, self.walls[wallNum].shape)

        print(x, offset)
				wallNum = wallNum+1
			end


			if(r==0 and g==255 and b==0) then
				--start of vertical line. iterate downwards to find lower bound.
				offset = 0
				while (r==0 and g==255 and b==0) and y + offset + 1 <self.collisions:getHeight() do
					self.collisions:setPixel(x-1,y + offset,0,0,0,0)
					offset = offset+1
					r,g,b,a = self.collisions:getPixel( x-1, y + offset)
				end
				self.collisions:setPixel(x-1,y+offset-1,0,0,0,0)
				self.walls[wallNum] = {}
				self.walls[wallNum].body = love.physics.newBody(world, x, y+self.collisions:getWidth()/2)
				self.walls[wallNum].shape = love.physics.newRectangleShape(1, offset)
				self.walls[wallNum].fixture = love.physics.newFixture(self.walls[wallNum].body, self.walls[wallNum].shape)

				wallNum = wallNum+1
			end

			if(r==0 and g==246 and b==255) then
				--start of vertical line. iterate downwards to find lower bound.
				offset = 0
				while (r==0 and g==246 and b==255) and y + offset + 1 <self.collisions:getHeight() and x + offset + 1 <self.collisions:getWidth() do
					self.collisions:setPixel(x + offset,y + offset,0,0,0,0)
					offset = offset+1
					r,g,b,a = self.collisions:getPixel( x + offset, y + offset)
				end
				self.collisions:setPixel(x,y+offset-1,0,0,0,0)
				self.walls[wallNum] = {}
				self.walls[wallNum].body = love.physics.newBody(world, x+offset/2, y+offset/2)
				self.walls[wallNum].shape = love.physics.newRectangleShape(1, offset*1.4142)
				self.walls[wallNum].fixture = love.physics.newFixture(self.walls[wallNum].body, self.walls[wallNum].shape)
				self.walls[wallNum].body:setAngle(3*math.pi/4)

				wallNum = wallNum+1
			end

			if(r==0 and g==0 and b==255) then
				--start of vertical line. iterate downwards to find lower bound.
				offset = 0
				while (r==0 and g==0 and b==255) and y + offset + 1 <self.collisions:getHeight() and x - offset - 1 >0 do
					self.collisions:setPixel(x - offset,y + offset,0,0,0,0)
					offset = offset+1
					r,g,b,a = self.collisions:getPixel( x - offset, y + offset)
				end
				self.collisions:setPixel(x,y+offset-1,0,0,0,0)
				self.walls[wallNum] = {}
				self.walls[wallNum].body = love.physics.newBody(world, x+offset/2, y+offset/2)
				self.walls[wallNum].shape = love.physics.newRectangleShape(1, offset*1.4142)
				self.walls[wallNum].fixture = love.physics.newFixture(self.walls[wallNum].body, self.walls[wallNum].shape)
				self.walls[wallNum].body:setAngle(math.pi/4)

				wallNum = wallNum+1
			end






			--pixels[#pixels + 1] = pixel
		end
  end
end

function Map:add_wall_from_path(value)
  if value.sub(value, 1, 1) == 'm' or value.sub(value, 1, 1) == 'M' then
    local wall = {}

    wall.vertices = {}

    local DEFAULT = 0
    local MOVE = 1
    local LINE = 2
    local MOVE_ABS = 3
    local LINE_ABS = 4

    local status = DEFAULT


    local SET_X = 1
    local SET_Y = 2
    local set_status = 0

    local x = 0
    local y = 0

    for token in string.gmatch(value, "([^ ]+)") do
      --print(token)
      if token == "m" or token == "M" then
        if token == "m" then
          status = MOVE
        else
          status = MOVE_ABS
        end
      elseif token == "l" or token == "L" then
        if token == "l" then
          status = LINE
        else
          status = LINE_ABS
        end
      elseif token == "h" or token == "H" then
        set_status = SET_X
        if token == "h" then
          status = LINE
        else
          status = LINE_ABS
        end

      elseif token == "v" or token == "V" then
        set_status = SET_Y

        if token == "v" then
          status = LINE
        else
          status = LINE_ABS
        end

      elseif token == "Z" or token == "z" then
        -- ignore
      elseif string.match(token, ",") ~= nil then
        local xy = string.gmatch(token, "([^,]+)")

        if status == MOVE_ABS or status == LINE_ABS then
          x = tonumber(xy())
          y = tonumber(xy())
        elseif status == MOVE or status == LINE then
          x = x + tonumber(xy())
          y = y + tonumber(xy())
        end

        table.insert(wall.vertices, x*3.846153846153846)
        table.insert(wall.vertices, y*3.846153846153846)
        --print(x*3.846153846153846)
        --print(y*3.846153846153846)
        --print(#wall.vertices)
      elseif string.match(token, ".*") ~= nil then
        if set_status == SET_X then
          if status == MOVE_ABS or status == LINE_ABS then
            x = tonumber(token)
          else
            x = x + tonumber(token)
          end
        elseif set_status == SET_Y then
          if status == MOVE_ABS or status == LINE_ABS then
            y = tonumber(token)
          else
            y = y + tonumber(token)
          end
        end
        table.insert(wall.vertices, x*3.846153846153846)
        table.insert(wall.vertices, y*3.846153846153846)
        --print(x*3.846153846153846)
        --print(y*3.846153846153846)
        --print(#wall.vertices)
      end
    end

    -- print(#wall.vertices)

    --local gm = string.gmatch(value, '[-+]?[0-9]*%.?[0-9]+,[-+]?[0-9]*%.?[0-9]+')
    --for pair in gm do
    --  local xy = string.gmatch(pair, "([^,]+)")
    --  local x = xy()
    --  local y = xy()
    --  table.insert(wall.vertices, tonumber(x))
    --  table.insert(wall.vertices, tonumber(y))
    --end

    for i, vertex in ipairs(wall.vertices) do
      print(vertex)
    end

    wall.body = love.physics.newBody(world, 0, 0)
    wall.shape = love.physics.newPolygonShape(wall.vertices)
    wall.fixture = love.physics.newFixture(wall.body, wall.shape)
    table.insert(self.walls, wall)
  end

  --print("Walls " .. #self.walls)
end

function Map:style_to_color(style)
  print("IN STYLE_TO_COLOR")
  for token in string.gmatch(style, "([^;]+)") do
    local keyvalue = string.gmatch(token, "([^:]+)")
    local key = keyvalue()
    local value = keyvalue()
    if key == "fill" then
      print(value)
      return value
    end

  end
end

function Map:circle_to_element(circle)
  if circle.color == "#ff0000" then
    -- use red as prisoner
    table.insert(self.prisoner_locations, {circle.x, circle.y})
  elseif circle.color == "#00ff00" then
    -- use green as guard
    table.insert(self.guard_locations, {circle.x, circle.y})
  end
end

function Map:path_to_element(path)
  if path.color == "#ff0000" then

    local door = self.walls[#self.walls]
    door.x = 0
    door.y = 0

    for i = 1, #door.vertices, 2 do
      local x = door.vertices[i]
      local y = door.vertices[i+1]
      door.x = door.x + x
      door.y = door.y + y
    end

    door.x = door.x / #door.vertices * 2
    door.y = door.y / #door.vertices * 2

    table.insert(self.doors, door)
  end
end

function Map:load2(svg_filename)
  local SLAXML = require 'slaxml'
  print(svg_filename)
  local xml = io.open(svg_filename):read('*all')

  self.walls = {}

  local in_path = false
  local in_image = false
  local in_circle = false
  local circle = {}
  local path = {}

  -- Specify as many/few of these as you like
  parser = SLAXML:parser{
    startElement = function(name,nsURI,nsPrefix)

      if name == "path" then
        in_path = true
        path = {}
      elseif name == "image" then
        in_image = true
      elseif name == "circle" then
        in_circle = true
        circle = {}
      end
    end, -- When "<foo" or <x:foo is seen

    attribute    = function(name,value,nsURI,nsPrefix)
      if in_path and (name == "d" or name == "style") then

        if name == "d" then
          self:add_wall_from_path(value)
          path.value = value
        elseif name == "style" then
          path.color = self:style_to_color(value)
        end
      end

      if in_image and name == "href" then
        self.map_sprite = love.graphics.newImage("map/" .. value)
      end

      if in_circle and (name == "cx" or name == "cy" or name == "style") then
        if name == "cx" then
          circle.x = tonumber(value)*3.846153846153846
        elseif name == "cy" then
          circle.y = tonumber(value)*3.846153846153846
        elseif name == "style" then
          circle.color = self:style_to_color(value)
        end
      end
    end, -- attribute found on current element

    closeElement = function(name,nsURI)
      if name == "path" then
        in_path = false
        self:path_to_element(path)
      elseif name == "image" then
        in_image = false
      elseif name == "circle" then
        in_circle = false
        self:circle_to_element(circle)
      end
    end, -- When "</foo>" or </x:foo> or "/>" is seen

    text         = function(text)

    end, -- text and CDATA nodes

    comment      = function(content)

    end, -- comments

    pi           = function(target,content)

    end, -- processing instructions e.g. "<?yes mon?>"
  }

  -- Ignore whitespace-only text nodes and strip leading/trailing whitespace from text
  -- (does not strip leading/trailing whitespace from CDATA)
  parser:parse(xml,{stripWhitespace=true})
end


function Map:draw()
    love.graphics.draw(self.map_sprite, 0, 0)

    for i, wall in ipairs(self.walls) do
      local x_prev = wall.vertices[1]
      local y_prev = wall.vertices[2]
      local x = wall.vertices[3]
      local y = wall.vertices[4]
      i = 5
      while x ~= nil do
        love.graphics.line(x_prev, y_prev, x, y)
        x = wall.vertices[i]
        y = wall.vertices[i+1]
        i = i + 2
      end
    end

end

return {Map = Map}
