Accompanied
-----------

This is a demo by David and Scott of a prison breakout game. While we did not finish much of the gameplay, the idea is that the more prisoners that are broken out, the more difficult to control the prisoners become.

This demo shows:
* How the prisoners would behave when there are many of them
* How keys can be stolen from guards
* How a level can be loaded from a simple SVG file

We hope to extend this in the future.

To run on windows:
double click on run.bat

To run on Linux:
Install love2d version 0.10.2 from the love2d website
Run the game by typing:
cd /path/to/game/love
/path/to/love .

To run on Mac:
You are on your own, sorry!
