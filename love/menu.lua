
local class = require "class"

local Menu = class(function(self, game_state)
  self.menu_items = {
    "New game",
    "Options",
    "Exit"
  }
  self.selected = 1
  self.game_state = game_state
  self.font = love.graphics.newFont(100)
  self.texts = {}

  for i,mi in ipairs(self.menu_items) do
    table.insert(self.texts, love.graphics.newText(self.font, mi))
  end
end)

function Menu:up()
  self.selected = self.selected - 1
  if self.selected < 1 then
    self.selected = #self.menu_items
  end
end

function Menu:down()
  self.selected = self.selected + 1
  if self.selected > #self.menu_items then
    self.selected = 1
  end
end

function Menu:select()
  if self.menu_items[self.selected] == "New game" then
    game_state:next()
    self.menu_items[self.selected] = "Continue"
  elseif self.menu_items[self.selected] == "Continue" then
    game_state:resume()
  elseif self.menu_items[self.selected] == "Options" then
    -- yep, whatever
  elseif self.menu_items[self.selected] == "Exit" then
    os.exit()
  end
end

function Menu:draw()
  for i, item in ipairs(self.menu_items) do
    -- self.texts[i]:clear()
    local color = {}
    if i == self.selected then
      color = {255, 0, 0}
    else
      color = {255, 255, 255}
    end

    self.texts[i]:set({
       color,
      item
    })

    love.graphics.draw(self.texts[i], 100, 10+i*150)

  end
end

return {Menu = Menu}
