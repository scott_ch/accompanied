
local class = require "class"

Card = class(function(self)
  self.flipped = false
  self.valid = true
end)

function Card:set_card_back(card_back, scale_x, scale_y)
  self.card_back = card_back
  self.scale_x = scale_x
  self.scale_y = scale_y
end

function Card:set_card_front(card_front, scale_x, scale_y)
  self.card_front = card_front
  self.scale_x = scale_x
  self.scale_y = scale_y
end

function Card:set_card_audio(audio)
  self.audio = audio
end

function Card:set_value(value)
  self.value = value
end

function Card:set_position(x, y)
  self.x = x
  self.y = y
end

function Card:set_bounds(width, height)
  self.width = width
  self.height = height
end

function Card:get_sprite()
  if self.flipped then
    return self.card_front
  else
    return self.card_back
  end
end

function Card:is_point_inside(x, y)
  if self.valid and x >= self.x and x <= self.x + self.width and
    y >= self.y and y <= self.y + self.height then
      return true
  end

  return false
end

function Card:flip()
  self.flipped = not self.flipped
end

function Card:remove()
  self.valid = false
end

return Card
